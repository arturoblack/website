
module.exports = function(grunt){
  // init config variablrd
  var config = grunt.file.readJSON('config.json');
  for(var i in config.scripts.lib){
    config.scripts.lib[i] = 'app/'+config.scripts.lib[i];
  }
  for(var j in config.scripts.app){
    config.scripts.app[j] = 'app/'+config.scripts.app[j];
  }
  for(var k in config.styles.lib){
    config.styles.lib[k] = 'app/'+config.styles.lib[k];
  }
  // project configuration
  grunt.initConfig({
    clean: {
      build: {
        src: [ 'dist' ]
      },
    },
    copy: {
      build: {
        cwd: 'source',
        src: [ '**' ],
        dest: 'build',
        expand: true
      },
    },
    uglify: {
      options: {
        mangle: false,
        compress: {
          drop_console: true
        }
      },
      js: {
        files:{
          'dist/scripts/lib.min.js': config.scripts.lib,
          'dist/scripts/app.min.js': config.scripts.app,
        }
      }
    },
    cssmin: {
      options: {
        mergeIntoShorthands: false,
        roundingPrecision: -1
      },
      target: {
        files: {
          'dist/styles/lib.min.css': config.styles.lib
        }
      }
    },
    jade: {
      debug: {
        options: {
          pretty: true,
          data: {
            scripts: config.scripts,
            styles: config.styles,
            data: config.data,
            env: 'dev'
          }
        },
        files: {
          'app/index.html': 'app/templates/jade/index.jade'
        }
      },
      release: {
        options: {
          pretty: false,
          data: {
            scripts: config.scripts,
            styles: config.styles,
            data: config.data,
            env: 'prod'
          }
        },
        files: {
          'dist/index.html': 'app/templates/jade/index.jade'
        }
      }
    },
    stylus: {
      debug: {
        options: {
          compress: false
        },
        files: {
          'app/styles/app.css': config.styles.app
        }
      },
      release: {
        options: {
          compress: true
        },
        files: {
          'dist/styles/app.min.css': config.styles.app
        }
      }
    },
    watch: {
      stylesheets: {
        files: ['app/styles/*.styl'], //config.styles.app,
        tasks: ['stylus:debug'],
        options: {
            interrupt: true
        }
      },
      jade: {
        files: ['./app/templates/jade/*.jade',
                './app/templates/jade/includes/*.jade',
                './app/templates/jade/layouts/*.jade'],
        tasks: ['jade:debug'],
        options: {
            interrupt: true
        }
      },
    },
  });

  //load modules
  grunt.loadNpmTasks('grunt-contrib-clean');
  grunt.loadNpmTasks('grunt-contrib-copy');
  grunt.loadNpmTasks('grunt-contrib-jade');
  grunt.loadNpmTasks('grunt-contrib-stylus');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-cssmin');
  //register tasks
  grunt.registerTask('default', ['uglify', 'cssmin']);
  grunt.registerTask('debug','Build page Debug mode', ['jade:debug', 'stylus:debug']);
  grunt.registerTask('release','Build page release mode', ['jade:release', 'stylus:release', 'uglify', 'cssmin']);
  grunt.registerTask('build','Build page', ['jade', 'stylus']);
  grunt.registerTask('watch','Watch-Er', ['watch:stylesheets', 'watch:jade']);
};
