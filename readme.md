CURIOSITY WEBSITE
===

Sitio Web de Cuiosity:
---
Pagina para curiosity.la

Install:
---

Instalar dependencias globales.
```
#!bash

install global dependencies grunt-cli, jade, stylus, bower
```

Instalar paquetes del server(npm).
```
#!bash

npm install
```

Instalar dependencias del cliente(bower).
```
#!bash

bower install
```

Debug:
---

Compilación de desarrollo.
```
#!bash

grunt debug
```



Release:
---

Compilación de preducción.
```
#!bash

grunt release
```

Live Server:
---


```
#!bash

npm start
```

Compilación Live para Desarrollo:
---
Falta implementar :(